import os
from django.contrib.gis.utils import LayerMapping
from .models import parcel_centroid

# Dictionary for parcel_centroid model
parcel_centroid_mapping = {
    'name': 'Name',
    'geom': 'MULTIPOINT',
}

# Defining path for the shapefile
centroid_shp = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data/test_data.shp'))


# Uploading data into the model
def run(verbose=True):
    lm = LayerMapping(parcel_centroid, str(centroid_shp), parcel_centroid_mapping, transform=False,
                      encoding='iso-8859-1')
    lm.save(strict=True, verbose=verbose)
